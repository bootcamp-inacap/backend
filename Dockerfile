FROM python:3.7-alpine

# install Alpine requirements
RUN apk add build-base 
RUN apk add gcc

# install Python requirements
RUN pip install pipenv
COPY Pipfile Pipfile.lock /tmp/
RUN cd tmp && pipenv install --deploy --system 

ENV PYTHONUNBUFFERED 1

# copy application files
COPY . /app
WORKDIR /app
