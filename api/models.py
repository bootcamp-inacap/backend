from django.db import models

class CrimeEvent(models.Model):
    latitude = models.FloatField()
    longitude = models.FloatField()
    time = models.TimeField()
