from django.db import transaction
from django.core.management.base import BaseCommand, CommandError

from api.models import CrimeEvent as CrimeEventModel

from dataclasses import dataclass
from datetime import time

import numpy as np

@dataclass
class Point:
    lat: float
    long: float

@dataclass
class CrimeEvent():
    location: Point
    time: time

def get_num_crimes_per_hour(size, *peaks):
    """
    This function will return an array containing a random number of 
    crimes per hour of a day (24 data points) with a distribution like this:

                            ####                     
          ##              ######                         
          ####          ############                           
        ########      ##############                               
        ##########    ################                                 
        ##########  ##################                                 
      ##################################                                     
      ##################################                                     
      ####################################                                     
      ######################################                                     
    ########################################                                       
    ########################################                  ##                     
    ##########################################              ####                         
    ############################################        ########                               
    ############################################        ########                               
    ################################################  ##########                                     
    ############################################################                                       
    ############################################################                                       
    0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23

    In this example we have two normally distributed peaks:
    one at 03:00 hours and the other one at 12:00 hours
    """
    peaks_dist = []
    for p in peaks:
        p_dist = np.random.normal(loc=p, scale=3, size=size)
        peaks_dist.append(p_dist)
    total = []
    for pd in peaks_dist:
        total = np.append(total, pd)
    total = np.mod(total, 24)
    num_crimes_per_hour, _ = np.histogram(total, range(0, 25))
    return num_crimes_per_hour

def crimes_per_day_generator(num_crimes_per_hour, mean, cov):
    """
    This generator function will yield random crime events for a certain region
    following a spatial multivariate normal distribution and distributed according
    to the num_crimes_per_hour argument for temporal dispertion.
    """
    for hour, crimes in enumerate(num_crimes_per_hour):
        lats, longs = np.random.multivariate_normal(mean, cov, size=crimes).T
        for lat, long in zip(lats, longs):
            yield CrimeEvent(Point(lat, long), time(hour=hour))

def clear_previous_crime_data():
    #pylint: disable=no-member
    CrimeEventModel.objects.all().delete()

DISTRIBUTIONS_PARAMS = [
    {
        'mean': [-23.6523055, -70.3925225],
        'cov': [[0.00001, 0], [0, 0.00001]],
        'time_distribution': get_num_crimes_per_hour(500, 3, 12)
    },
    {
        'mean': [-23.669487, -70.393958],
        'cov': [[0.00001, 0], [0, 0.00001]],
        'time_distribution': get_num_crimes_per_hour(1000, 21)
    }
]

class Command(BaseCommand):
    @transaction.atomic
    def handle(self, *args, **options):
        clear_previous_crime_data()

        crime_distributions = [ crimes_per_day_generator(params['time_distribution'], params['mean'], params['cov']) for params in DISTRIBUTIONS_PARAMS ]

        for crimes in crime_distributions:
            for crime in crimes:
                CrimeEventModel.objects.create(latitude=crime.location.lat, longitude=crime.location.long, time=crime.time)

        self.stdout.write(self.style.SUCCESS('Successfully loaded crime data'))
