from django.conf.urls import url
from django.urls import include
from rest_framework.routers import DefaultRouter

from api.views import CrimeEventViewSet

router = DefaultRouter()
router.register(r'crime-event', CrimeEventViewSet, 'crime-event')

urlpatterns = [
    url(r'^', include(router.urls)),
]
