from rest_framework import viewsets

from api.models import CrimeEvent
from api.serializers import CrimeEventSerializer

class CrimeEventViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = CrimeEventSerializer

    def get_queryset(self):
        start = self.request.query_params.get('start', None)
        end = self.request.query_params.get('end', None)
        #pylint: disable=no-member
        if start and end:
            return CrimeEvent.objects.filter(time__range=[start, end])
        if start and not end:
            return CrimeEvent.objects.filter(time__gte=start)
        if end and not start:
            return CrimeEvent.objects.filter(time__lte=end)
        
        return CrimeEvent.objects.all()