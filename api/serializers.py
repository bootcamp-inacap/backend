from rest_framework import serializers

from api.models import CrimeEvent

class CrimeEventSerializer(serializers.ModelSerializer):
    class Meta:
        model = CrimeEvent
        fields = '__all__'